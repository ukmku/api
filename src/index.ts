// Dependency

import express, { Application, Request, Response } from 'express'
import { config as dotenv } from 'dotenv'
import Mongoose from './config/mongoose'
import compression from 'compression'
import bodyParser from 'body-parser'
import helmet from 'helmet'
import morgan from 'morgan'
import path from 'path'
import cors from 'cors'

import communityCategoryRoutes from './routes/communityCategory.routes'
import personalityResultRoutes from './routes/personalityResult.routes'
import personalityTestRoutes from './routes/personalityTest.routes'
import personalityRoutes from './routes/personality.routes'
import communityRoutes from './routes/community.routes'
import userRoutes from './routes/user.routes'
import authRoutes from './routes/auth.routes'
import interestRoutes from './routes/interest.routes'
import interestTestRoutes from './routes/interestTest.routes'
import interestResultRoutes from './routes/interestResult.routes'
import rangkingRoutes from './routes/rangking.routes'

class App {
  public app: Application

  constructor() {
    this.app = express()
    this.plugins()
    this.routes()
  }

  public plugins(): void {
    this.app.use(bodyParser.json({ limit: '50mb' }))
    this.app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }))
    this.app.use(express.static(path.join(__dirname, '../public')))
    this.app.use(morgan('dev'))
    this.app.use(compression())
    this.app.use(helmet())
    this.app.use(cors())
    dotenv()
  }

  public routes(): void {
    this.app.route('/').get((req: Request, res: Response) => {
      res.sendFile(path.join(__dirname, '../public/pages/index.html'))
    })

    this.app.route('/api/hello').get((req: Request, res: Response) => {
      res.send('Hello World!')
    })

    this.app.use('/api/v1/auth', authRoutes)
    this.app.use('/api/v1/users', userRoutes)
    this.app.use('/api/v1/interests', interestRoutes)
    this.app.use('/api/v1/interestTests', interestTestRoutes)
    this.app.use('/api/v1/interestResults', interestResultRoutes)
    this.app.use('/api/v1/personalities', personalityRoutes)
    this.app.use('/api/v1/personalityTests', personalityTestRoutes)
    this.app.use('/api/v1/personalityResults', personalityResultRoutes)
    this.app.use('/api/v1/communityCategories', communityCategoryRoutes)
    this.app.use('/api/v1/communities', communityRoutes)
    this.app.use('/api/v1/rangkings', rangkingRoutes)
  }

  public start(): void {
    const port: number = 8080
    this.app.listen(port, () => {
      console.log(
        `${process.env.APP_NAME} Service is listening on port ${port}`
      )
    })
  }
}

const mongoose = new Mongoose()
const app = new App()
app.start()
