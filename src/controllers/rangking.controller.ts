import { Request, Response } from 'express'
import Helper from '../utils/helper'
import User from '../models/User'
import Rangking from '../models/Rangking'
import _ from 'lodash'

class rangkingController {
  store = async (req: Request, res: Response): Promise<Response> => {
    let names = [
      'Olahraga',
      'Seni',
      'Kerohanian',
      'Bahasa dan Budaya',
      'Alam',
      'IT',
      'Sosial',
      'Kewirausahaan',
    ]

    const { id } = req.app.locals.user
    const { personality, department, interest } = req.body
    const [one, two, three] = interest
    const getPersonality = Helper.getRowPersonality(personality)
    const getInterestOne = Helper.getRowInterest(one)
    const getInterestTwo = Helper.getRowInterest(two)
    const getInterestThree = Helper.getRowInterest(three)
    const getDepartment = Helper.getRowDepartment(department)

    let merged = names.map((name, index) => {
      return {
        name,
        personality: getPersonality[index],
        interestOne: getInterestOne[index],
        interestTwo: getInterestTwo[index],
        interestThree: getInterestThree[index],
        department: getDepartment[index],
      }
    })

    const getMinMax = Helper.getMinMax(merged)
    const normalisasiNilai = _.map(merged, (nilai) =>
      Helper.normalization(nilai, getMinMax)
    )
    const hitungBobotPeringkat = _.map(normalisasiNilai, (nilai) =>
      Helper.rangking(nilai)
    )
    const getRangking = _.sortBy(hitungBobotPeringkat, 'total').reverse()

    // Mengecek apakah user pernah tes spk
    const myRangking = await Rangking.findOne({ createdBy: id })

    // res.send(getRangking)

    if (!myRangking) {
      // jika belum pernah akan menyimpan data baru
      const ranking = new Rangking({
        createdBy: id,
        ranking: getRangking,
        los: "Ss"
      })
      await ranking.save()

      return res.send({
        success: true,
        messages: 'OK',
        data: ranking,
      })
    } else {
      const ranking = await Rangking.update(
        { createdBy: id },
        {
          ranking: getRangking,
        }
      )

      const rangkingAfterUpdate = await Rangking.findOne({
        createdBy: id,
      })
      return res.send({
        success: true,
        messages: 'OK',
        data: rangkingAfterUpdate,
      })
    }
  }
}

export default new rangkingController()
