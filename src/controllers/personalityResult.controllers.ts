import PersonalityResultService from '../services/personalityResult.service'
import PersonalityResult from '../models/PersonalityResult'
import UserService from '../services/user.service'
import { Request, Response } from 'express'
import Helper from '../utils/helper'
import User from '../models/User'

class personalityResultController {
  myPersonality = async (req: Request, res: Response): Promise<Response> => {
    const services: PersonalityResultService = new PersonalityResultService(req)
    const personalityResult = await services.myPersonality()

    if(!personalityResult){
      return res.send({
        success: true,
        messages: 'User never does the test',
        data: personalityResult,
      })
    }

    return res.send({
      success: true,
      messages: 'OK',
      data: personalityResult,
    })
  }

  index = async (req: Request, res: Response): Promise<Response> => {
    const services: PersonalityResultService = new PersonalityResultService(req)
    const personalityResult = await services.getAll()
    return res.send({
      success: true,
      messages: 'OK',
      data: personalityResult,
    })
  }

  show = async (req: Request, res: Response): Promise<Response> => {
    const { id } = req.params
    const services: PersonalityResultService = new PersonalityResultService(req)
    const personalityResult = await services.showById(id)

    if (!personalityResult) {
      return res.send({
        success: false,
        messages: 'Data not found',
      })
    }

    return res.send({
      success: true,
      messages: 'OK',
      data: personalityResult,
    })
  }

  store = async (req: Request, res: Response): Promise<Response> => {
    const { results } = req.body
    const { id } = req.app.locals.user

    const Resultservices: PersonalityResultService = new PersonalityResultService(
      req
    )

    const myPersonality = await Resultservices.myPersonality()

    const personalityResultArr: Array<string> = results
    personalityResultArr.sort()
    const personalityReport = Helper.compressedArray(personalityResultArr)

    const Ekstrovert = personalityReport[0].count
    const Introvert = personalityReport[2].count
    const Sensing = personalityReport[6].count
    const Intuition = personalityReport[3].count
    const Feeling = personalityReport[1].count
    const Thinking = personalityReport[7].count
    const Judging = personalityReport[4].count
    const Perceiving = personalityReport[5].count

    let a = null
    let b = null
    let c = null
    let d = null

    if (Introvert > Ekstrovert) {
      a = 'I'
    } else {
      a = 'E'
    }

    if (Sensing > Intuition) {
      b = 'S'
    } else {
      b = 'N'
    }

    if (Thinking > Feeling) {
      c = 'T'
    } else {
      c = 'F'
    }

    if (Judging > Perceiving) {
      d = 'J'
    } else {
      d = 'P'
    }

    const PersonalityResultData = a + b + c + d
    const personality = PersonalityResultData

    if (!myPersonality) {
      const personalityResult = new PersonalityResult(
        await Resultservices.store(id, personality, personalityReport)
      )
      return res.send({
        success: true,
        messages: 'OK',
        data: personalityResult,
      })
    } else {
      const personalityResult = await Resultservices.update(
        id,
        personality,
        personalityReport
      )
      const myPersonalityAfterUpdate = await Resultservices.myPersonality()
      return res.send({
        success: true,
        messages: 'OK',
        data: myPersonalityAfterUpdate,
      })
    }
  }
}

export default new personalityResultController()
