import { Request, Response } from 'express'
import User, { IUser } from '../models/User'
import Personality, { IPersonality } from '../models/Personality'
import PersonalityResult from '../models/PersonalityResult'

import UserService from '../services/user.service'
import base64MimeType from '../utils/base64MimeType'
import checkImageExt from '../utils/checkImageExt'
import fs from 'fs'
import isBase64 from 'is-base64'
import InterestResultService from '../services/interestResult.service'
import InterestResults from '../models/InterestResults'

const base64Img = require('base64-img')

class userController {
  profile = async (req: Request, res: Response): Promise<Response> => {
    const { id } = req.app.locals.user
    const user = new User(await User.findOne({ _id: id }))

    // find personality
    const personality = new PersonalityResult(
      await PersonalityResult.findOne({ createdBy: id })
    )

    // find interest
    const interest = new InterestResults(
      await InterestResults.findOne({ createdBy: id })
    )

    let avatar =
      user.avatar === null
        ? null
        : `${req.protocol}://${req.get('host')}${user.avatar}`

    let data = new Object()
    Object.assign(data, {
      id: user.id,
      name: user.name,
      avatar,
      email: user.email,
      nim: user.nim,
      department: user.department,
      phone: user.phone,
      personality: personality.personality,
      interest,
    })

    return res.send({
      success: true,
      messages: 'OK',
      data,
    })
  }

  update = async (req: Request, res: Response): Promise<Response> => {
    const { _id, name, nim, department, phone } = req.body
    const services: UserService = new UserService(req)
    const userAuth = new User(
      await services.update(_id, nim, name, department, phone)
    )

    return res.json({
      success: true,
      messages: 'OK',
    })
  }

  avatar = async (req: Request, res: Response): Promise<Response> => {
    const { _id } = req.app.locals.user
    const { avatar } = req.body
    const services: UserService = new UserService(req)
    const userAuth = new User(await services.Auth())

    // Image Validation
    if (checkImageExt(base64MimeType(req.body.avatar))) {
      if (!isBase64(avatar, { mimeRequired: true })) {
        return res
          .status(400)
          .json({ success: false, message: 'invalid base64' })
      }
    } else {
      return res
        .status(400)
        .json({ success: false, message: 'invalid datatypes' })
    }

    // replace oldAvatar
    if (userAuth.avatar !== null) {
      const oldAvatar = userAuth.avatar
      fs.unlink(`./public/${oldAvatar}`, async (err) => {
        if (err) {
          return res.status(400).json({ success: false, message: err.message })
        }
      })
    }

    // upload an Avatar
    const avatarImage = base64Img.img(
      avatar,
      './public/images/avatar',
      Date.now(),
      async (err: any, filepath: any) => {
        if (err) {
          return res.status(400).json({ success: false, message: err.message })
        }

        const filename = filepath.split('\\').pop().split('/').pop()
        const filenameWithoutHost: string = `/images/avatar/${filename}`
        const fileNameWithHost: string = `${req.protocol}://${req.get(
          'host'
        )}${filenameWithoutHost}`

        const services: UserService = new UserService(req)
        const user = new User(
          await services.updateAvatar(_id, filenameWithoutHost)
        )

        const userObj = (user: IUser) => {
          user.avatar = fileNameWithHost
          return user
        }

        return res.json({
          success: true,
          messages: 'OK',
          data: userObj(user),
        })
      }
    )

    return avatarImage
  }

  /*
  update = async (req: Request, res: Response): Promise<Response> => {
    const { avatar, email, name, nim, department, phone } = req.body
    const services: UserService = new UserService(req)
    const userAuth = new User(await services.Auth())

    // Image Validation
    if (checkImageExt(base64MimeType(req.body.avatar))) {
      if (!isBase64(avatar, { mimeRequired: true })) {
        return res
          .status(400)
          .json({ success: false, message: 'invalid base64' })
      }
    } else {
      return res
        .status(400)
        .json({ success: false, message: 'invalid datatypes' })
    }

    // replace oldAvatar
    if (userAuth.avatar !== null) {
      const oldAvatar = userAuth.avatar
      fs.unlink(`./public/${oldAvatar}`, async (err) => {
        if (err) {
          return res.status(400).json({ success: false, message: err.message })
        }
      })
    }

    // upload an Avatar
    const avatarImage = base64Img.img(
      avatar,
      './public/images',
      Date.now(),
      async (err: any, filepath: any) => {
        if (err) {
          return res.status(400).json({ success: false, message: err.message })
        }

        const filename = filepath.split('\\').pop().split('/').pop()
        const filenameWithoutHost: string = `/images/${filename}`
        const fileNameWithHost: string = `${req.get(
          'host'
        )}${filenameWithoutHost}`

        const services: UserService = new UserService(req)
        const user = new User(
          await services.update(
            email,
            filenameWithoutHost,
            nim,
            name,
            department,
            phone
          )
        )

        const userObj = (user: IUser) => {
          user.avatar = fileNameWithHost
          return user
        }

        return res.json({
          status: res.statusCode,
          success: true,
          user: userObj(user),
        })
      }
    )

    return avatarImage
  }
*/
}

export default new userController()
