import { Request, Response } from 'express'
import base64MimeType from '../utils/base64MimeType'
import checkImageExt from '../utils/checkImageExt'
import fs from 'fs'
import isBase64 from 'is-base64'
import CommunityService from '../services/community.service'
import Community, { ICommunity } from '../models/Community'
import Helper from '../utils/helper'

const base64Img = require('base64-img')

class communityController {
  profile = async (req: Request, res: Response): Promise<Response> => {
    console.log(req.app.locals.community)
    const services: CommunityService = new CommunityService(req)
    const community = new Community(await services.Auth())
    console.log(community)
    const communityObj = (community: ICommunity) => {
      community.logo = `${req.protocol}://${req.get('host')}${community.logo}`
      return community
    }

    if (community.logo === null) {
      return res.send({
        success: true,
        messages: 'OK',
        data: community,
      })
    }
    return res.send({
      success: true,
      messages: 'OK',
      data: communityObj(community),
    })
  }

  index = async (req: Request, res: Response): Promise<Response> => {
    const services: CommunityService = new CommunityService(req)
    const community = await services.index()
    const communityMapped = community.map((data) => {
      data.logo = `${req.protocol}://${req.get('host')}${data.logo}`
      return data
    })

    return res.json({
      success: true,
      messages: 'OK',
      data: communityMapped,
    })
  }

  getByCategory = async (req: Request, res: Response): Promise<Response> => {
    const { category } = req.params
    const services: CommunityService = new CommunityService(req)
    const community = await services.findByCategory(category)
    return res.json({
      success: true,
      messages: 'OK',
      data: community,
    })
  }

  store = async (req: Request, res: Response): Promise<Response> => {
    const services: CommunityService = new CommunityService(req)
    const community = await services.store()

    return res.json({
      success: true,
      messages: 'OK',
      data: community,
    })
  }

  show = async (req: Request, res: Response): Promise<Response> => {
    const { id } = req.params
    const isValid = Helper.isValid(id)

    if (!isValid) {
      return res.send({
        success: false,
        messages: 'Community not found',
      })
    }

    const services: CommunityService = new CommunityService(req)
    const community = await services.show()

    if (!community) {
      return res.json({
        success: true,
        messages: 'OK',
        data: 'Community not found',
      })
    }

    return res.json({
      success: true,
      messages: 'OK',
      data: community,
    })
  }

  update = async (req: Request, res: Response): Promise<Response> => {
    const { _id } = req.app.locals.community
    const {
      logo,
      description,
      schedule,
      activities,
      achievements,
      contactPerson,
      email,
      instagram,
      twitter,
      youtube,
      facebook,
      website,
    } = req.body
    const services: CommunityService = new CommunityService(req)
    const userAuth = new Community(await services.Auth())

    // Image Validation
    if (checkImageExt(base64MimeType(req.body.logo))) {
      if (
        !isBase64(logo, {
          mimeRequired: true,
        })
      ) {
        return res.status(400).json({
          success: false,
          message: 'invalid base64',
        })
      }
    } else {
      return res.status(400).json({
        success: false,
        message: 'invalid datatypes',
      })
    }

    // replace oldAvatar
    if (userAuth.logo !== null) {
      const oldAvatar = userAuth.logo
      fs.unlink(`./public/${oldAvatar}`, async (err) => {
        if (err) {
          return res.status(400).json({
            success: false,
            message: err.message,
          })
        }
      })
    }

    // upload an Avatar
    const avatarImage = base64Img.img(
      logo,
      './public/images/communities/logo',
      Date.now(),
      async (err: any, filepath: any) => {
        if (err) {
          return res.status(400).json({
            success: false,
            message: err.message,
          })
        }

        const filename = filepath.split('\\').pop().split('/').pop()
        const filenameWithoutHost: string = `/images/communities/logo/${filename}`
        const fileNameWithHost: string = `${req.protocol}://${req.get(
          'host'
        )}${filenameWithoutHost}`

        const services: CommunityService = new CommunityService(req)
        const community = await services.update(
          filenameWithoutHost,
          description,
          schedule,
          activities,
          achievements,
          contactPerson,
          email,
          instagram,
          twitter,
          youtube,
          facebook,
          website
        )

        const communityObj = (community: ICommunity) => {
          community.logo = fileNameWithHost
          return community
        }

        return res.json({
          success: true,
          messages: 'OK',
          data: communityObj(community),
        })
      }
    )

    return avatarImage
  }

  destroy = async (req: Request, res: Response): Promise<Response> => {
    const { id } = req.params
    const isValid = Helper.isValid(id)

    if (!isValid) {
      return res.send({
        success: false,
        messages: 'Community not found',
      })
    }

    const services: CommunityService = new CommunityService(req)
    const community = await services.destroy()

    if (!community) {
      return res.json({
        success: true,
        messages: 'OK',
        data: 'Community not found',
      })
    }

    return res.json({
      success: true,
      messages: 'OK',
      data: community,
    })
  }
}

export default new communityController()
