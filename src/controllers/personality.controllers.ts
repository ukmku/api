import { Request, Response } from 'express'

import PersonalityService from '../services/personality.service'
import Personality, { IPersonality } from '../models/Personality'

class personalityController {
  index = async (req: Request, res: Response): Promise<Response> => {
    const services: PersonalityService = new PersonalityService(req)
    const personality = await services.getAll()

    const personalityMapped = personality.map((data) => {
      data.image = `${req.protocol}://${req.get('host')}${data.image}`
      return data
    })

    return res.send({
      success: true,
      messages: 'OK',
      data: personalityMapped,
    })
  }

  show = async (req: Request, res: Response): Promise<Response> => {
    const services: PersonalityService = new PersonalityService(req)
    const personality = await services.show()

    const personalityObj = (data: IPersonality) => {
      data.image = `${req.protocol}://${req.get('host')}${data.image}`
      return data
    }

    if (!personality) {
      return res.status(404).send({
        success: false,
        messages: 'Data not found',
      })
    }
    return res.send({
      success: true,
      messages: 'OK',
      data: personalityObj(personality),
    })
  }
}

export default new personalityController()
