import { Request, Response } from 'express'

import InterestService from '../services/interest.service'
import Interest, { IInterest } from '../models/Interest'

class interestController {
  index = async (req: Request, res: Response): Promise<Response> => {
    const services: InterestService = new InterestService(req)
    const interest = await services.getAll()

    const interestMapped = interest.map((data) => {
      data.image = `${req.protocol}://${req.get('host')}${data.image}`
      return data
    })

    return res.send({
      success: true,
      messages: 'OK',
      data: interestMapped,
    })
  }

  show = async (req: Request, res: Response): Promise<Response> => {
    const services: InterestService = new InterestService(req)
    const interest = await services.show()

    const interestObj = (data: IInterest) => {
      data.image = `${req.protocol}://${req.get('host')}${data.image}`
      return data
    }

    console.log(interestObj)

    if (!interest) {
      return res.status(404).send({
        success: false,
        messages: 'Data not found',
      })
    }
    return res.send({
      success: true,
      messages: 'OK',
      data: interestObj(interest),
    })
  }

  getThree = async (req: Request, res: Response): Promise<Response> => {
    let arr: Array<string> = ["Konvensional", "Sosial", "Artistik"]
    arr = ['Sosial', 'Konvensional', 'Artistik']
    const services: InterestService = new InterestService(req)
    const interest = await services.getThree(arr)
    return res.send({
      success: true,
      messages: 'OK',
      data: interest,
    })
  }
}

export default new interestController()
