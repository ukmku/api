import CommunityCategory from '../models/CommunityCategory'
import CommunityCategoryService from '../services/communityCategory.service'
import { Request, Response } from 'express'

class communityCategoryController {
  index = async (req: Request, res: Response): Promise<Response> => {
    const services: CommunityCategoryService = new CommunityCategoryService(req)
    const communityCategory = await services.index()

    return res.send({
      success: true,
      messages: 'OK',
      data: communityCategory,
    })
  }

  store = async (req: Request, res: Response): Promise<Response> => {
    const { name } = req.body
    const services: CommunityCategoryService = new CommunityCategoryService(req)
    const communityCategory = await services.store(name)

    return res.send({
      success: true,
      messages: 'OK',
      data: communityCategory,
    })
  }

  show = async (req: Request, res: Response): Promise<Response> => {
    const services: CommunityCategoryService = new CommunityCategoryService(req)
    const communityCategory = await services.show()

    return res.send({
      success: true,
      messages: 'OK',
      data: communityCategory,
    })
  }

  update = async (req: Request, res: Response): Promise<Response> => {
    const services: CommunityCategoryService = new CommunityCategoryService(req)
    const communityCategory = await services.update()

    return res.send({
      success: true,
      messages: 'OK',
      data: communityCategory,
    })
  }

  delete = async (req: Request, res: Response): Promise<Response> => {
    const services: CommunityCategoryService = new CommunityCategoryService(req)
    const communityCategory = await services.destroy()

    return res.send({
      success: true,
      messages: 'OK',
      data: communityCategory,
    })
  }
}

export default new communityCategoryController()
