import interestTestService from '../services/interestTest.service'
import InterestService from '../services/interest.service'
import { Request, Response } from 'express'
import Helper from '../utils/helper'
import User from '../models/User'
import Interest, { IInterest } from '../models/Interest'
import InterestResults, { IInterestResult } from '../models/InterestResults'

class interestResultController {
  store = async (req: Request, res: Response): Promise<Response> => {
    const { id } = req.app.locals.user
    const { results } = req.body

    const resultArr: Array<string> = results
    resultArr.sort()
    const report = Helper.compressedArray(resultArr)
    const reportSorted: [{ count: number; value: string }] = report.sort(
      Helper.compareValues('count', 'desc')
    )

    let interest: Array<string> = []
    reportSorted.forEach((datum) => interest.push(datum.value))
    console.log(interest.splice(3, 3))

    const services: InterestService = new InterestService(req)
    const result = await services.getThree(interest)

    const interestObj = result.map((data) => {
      data.image = `${req.protocol}://${req.get('host')}${data.image}`
      return data
    })

    // Mengecek apakah user pernah tes minat bakat
    const myInterest = await InterestResults.findOne({ createdBy: id })

    if (!myInterest) {
      // jika belum pernah akan menyimpan data baru
      const interestResult = new InterestResults({
        createdBy: id,
        interest,
        description: interestObj,
        interestReport: reportSorted,
      })
      await interestResult.save()

      return res.send({
        success: true,
        messages: 'OK',
        data: interestResult,
      })
    } else {
      const interestResult = await InterestResults.update(
        { createdBy: id },
        {
          interest,
          description: interestObj,
          interestReport: reportSorted,
        }
      )

      const interestAfterUpdate = await InterestResults.findOne({
        createdBy: id,
      })
      return res.send({
        success: true,
        messages: 'OK',
        data: interestAfterUpdate,
      })
    }

    console.log('interestResult')
  }
}

export default new interestResultController()
