import { Request, Response } from 'express'

import PersonalityTest from '../models/PersonalityTest'
import PersonalityTestService from '../services/personalityTest.service'

class personalityTestController {
  indexRandom = async (req: Request, res: Response): Promise<Response> => {
    const personalityTest = PersonalityTest.findRandom(
      {},
      {},
      { limit: 60 },
      (err, results) => {
        if (!err) {
          return res.send({
            success: true,
            messages: 'OK',
            data: results,
          })
        }
      }
    )

    return res.send(personalityTest)
  }

  showByModul = async (req: Request, res: Response): Promise<Response> => {
    const services: PersonalityTestService = new PersonalityTestService(req)
    const personalityTest = await services.showByModul()

    if (personalityTest.length === 0) {
      return res.status(404).send({
        success: false,
        messages: 'Data not found',
      })
    }

    return res.send({
      success: true,
      messages: 'OK',
      data: personalityTest,
    })
  }
}

export default new personalityTestController()
