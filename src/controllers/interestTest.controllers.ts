import { Request, Response } from 'express'

import InterestTest from '../models/InterestTest'
import InterestTestService from '../services/interestTest.service'

class interestTestController {
  indexRandom = async (req: Request, res: Response): Promise<Response> => {
    const interestTest = InterestTest.findRandom(
      {},
      {},
      { limit: 60 },
      (err, results) => {
        if (!err) {
          return res.send({
            success: true,
            messages: 'OK',
            data: results,
          })
        }
      }
    )

    return res.send(interestTest)
  }

  showByModul = async (req: Request, res: Response): Promise<Response> => {
    const services: InterestTestService = new InterestTestService(req)
    const interestTest = await services.showByModul()

    if (interestTest.length === 0) {
      return res.status(404).send({
        success: false,
        messages: 'Data not found',
      })
    }

    return res.send({
      success: true,
      messages: 'OK',
      data: interestTest,
    })
  }
}

export default new interestTestController()
