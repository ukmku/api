import base64MimeType from '../utils/base64MimeType'
import checkImageExt from '../utils/checkImageExt'
import CommunityMedia, { ICommunityMedia } from '../models/CommunityMedia'
import { Request, Response } from 'express'
import fs from 'fs'
import isBase64 from 'is-base64'
import Helper from '../utils/helper'

const base64Img = require('base64-img')

class communityMediaController {
  index = async (req: Request, res: Response): Promise<Response> => {
    const media = await CommunityMedia.find({})

    const mediaMapped = media.map((data) => {
      if (typeof data.image === 'undefined') {
        data.image = ''
      } else {
        data.image = `${req.protocol}://${req.get('host')}${data.image}`
      }
      return data
    })

    return res.send({
      success: true,
      messages: 'OK',
      data: mediaMapped,
    })
  }

  store = async (req: Request, res: Response): Promise<Response> => {
    const { image, video, title, description } = req.body
    const { id } = req.app.locals.community

    if (typeof image !== 'undefined') {
      // Image Validation
      if (checkImageExt(base64MimeType(image))) {
        if (!isBase64(image, { mimeRequired: true })) {
          return res
            .status(400)
            .json({ success: false, message: 'invalid base64' })
        }
      } else {
        return res
          .status(400)
          .json({ success: false, message: 'invalid datatypes for image' })
      }

      const avatarImage = base64Img.img(
        image,
        './public/images/communities/media',
        Date.now(),
        async (err: any, filepath: any) => {
          if (err) {
            return res
              .status(400)
              .json({ success: false, message: err.message })
          }

          const filename = filepath.split('\\').pop().split('/').pop()
          const filenameWithoutHost: string = `/images/communities/media/${filename}`
          const fileNameWithHost: string = `${req.protocol}://${req.get(
            'host'
          )}${filenameWithoutHost}`

          const media = new CommunityMedia({
            createdBy: id,
            image: filenameWithoutHost,
            video,
            title,
            description,
          })
          await media.save()

          const userObj = (media: ICommunityMedia) => {
            media.image = fileNameWithHost
            return media
          }

          return res.json({
            success: true,
            messages: 'OK',
            data: userObj(media),
          })
        }
      )

      return avatarImage
    } else {
      const media = new CommunityMedia({
        createdBy: id,
        video,
        image,
        title,
        description,
      })
      await media.save()

      return res.send({
        success: true,
        messages: 'OK',
        data: media,
      })
    }
  }

  show = async (req: Request, res: Response): Promise<Response> => {
    const { id } = req.params
    const media = await CommunityMedia.find({ _id: id })

    if (!media) {
      return res.send({
        success: false,
        messages: 'NOT FOUND',
      })
    }

    const mediaMapped = media.map((data) => {
      if (typeof data.image === 'undefined') {
        data.image = ''
      } else {
        data.image = `${req.protocol}://${req.get('host')}${data.image}`
      }
      return data
    })

    return res.send({
      success: true,
      messages: 'OK',
      data: mediaMapped,
    })
  }

  update = async (req: Request, res: Response): Promise<Response> => {
    const { id } = req.params
    const { image, video, title, description } = req.body
    const media = await CommunityMedia.findOne({ _id: id })

    if (!media) {
      return res.send({
        success: false,
        messages: 'Media not found',
      })
    }

    if (typeof image !== 'undefined') {
      // Image Validation
      if (checkImageExt(base64MimeType(image))) {
        if (!isBase64(image, { mimeRequired: true })) {
          return res
            .status(400)
            .json({ success: false, message: 'invalid base64' })
        }
      } else {
        return res
          .status(400)
          .json({ success: false, message: 'invalid datatypes for image' })
      }

      // replace oldAvatar
      if (media?.image !== null) {
        const oldAvatar = media?.image

        fs.unlink(`./public/${oldAvatar}`, async (err) => {
          if (err) {
            return res
              .status(400)
              .json({ success: false, message: err.message })
          }
        })
      }

      const avatarImage = base64Img.img(
        image,
        './public/images/communities/media',
        Date.now(),
        async (err: any, filepath: any) => {
          if (err) {
            return res
              .status(400)
              .json({ success: false, message: err.message })
          }

          const filename = filepath.split('\\').pop().split('/').pop()
          const filenameWithoutHost: string = `/images/communities/media/${filename}`
          const fileNameWithHost: string = `${req.protocol}://${req.get(
            'host'
          )}${filenameWithoutHost}`

          const media = await CommunityMedia.updateOne(
            { _id: id },
            {
              $set: {
                title,
                description,
                image: filenameWithoutHost,
              },
            }
          )

          return res.json({
            success: true,
            messages: 'OK',
            data: media,
          })
        }
      )

      return avatarImage
    } else {
      const media = await CommunityMedia.updateOne({ _id: id }, req.body)

      return res.json({
        success: true,
        messages: 'OK',
        data: media,
      })
    }
  }

  destroy = async (req: Request, res: Response): Promise<Response> => {
    const { id } = req.params
    const isValid = Helper.isValid(id)

    if (!isValid) {
      return res.send({
        success: false,
        messages: 'Media not found',
      })
    }

    const media = await CommunityMedia.findByIdAndDelete({ _id: id })

    // replace oldAvatar
    if (media?.image !== null) {
      const oldAvatar = media?.image
      fs.unlink(`./public/${oldAvatar}`, async (err) => {
        if (err) {
          return res.status(400).json({ success: false, message: err.message })
        }
      })
    }

    console.log(media?.image)

    if (!media) {
      return res.send({
        success: false,
        messages: 'Media not found',
      })
    }

    return res.send({
      success: true,
      messages: 'OK',
      data: media,
    })
  }

  getByAuth = async (req: Request, res: Response): Promise<Response> => {
    const { _id } = req.app.locals.community
    const media = await CommunityMedia.find({ createdBy: _id })

    if (!media) {
      return res.send({
        success: false,
        messages: 'NOT FOUND',
      })
    }

    const mediaMapped = media.map((data) => {
      if (typeof data.image === 'undefined') {
        data.image = ''
      } else {
        data.image = `${req.protocol}://${req.get('host')}${data.image}`
      }
      return data
    })

    return res.send({
      success: true,
      messages: 'OK',
      data: mediaMapped,
    })
  }

  showVideo = async (req: Request, res: Response): Promise<Response> => {
    const { _id } = req.app.locals.community
    const video = await CommunityMedia.find({ createdBy: _id })
      .where('video')
      .ne(null)

    return res.send({
      success: true,
      messages: 'OK',
      data: video,
    })
  }

  showImage = async (req: Request, res: Response): Promise<Response> => {
    const { _id } = req.app.locals.community
    const image = await CommunityMedia.find({ createdBy: _id })
      .where('image')
      .ne(null)

    const mediaMapped = image.map((data) => {
      if (typeof data.image === 'undefined') {
        data.image = ''
      } else {
        data.image = `${req.protocol}://${req.get('host')}${data.image}`
      }
      return data
    })

    return res.send({
      success: true,
      messages: 'OK',
      data: mediaMapped,
    })
  }
}

export default new communityMediaController()
