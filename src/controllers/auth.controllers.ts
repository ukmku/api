import { NextFunction, Request, Response, Router } from 'express'
import Community from '../models/Community'
import User from '../models/User'

import CommunityService from '../services/community.service'
import Authentication from '../utils/authentication'
import UserService from '../services/user.service'

class authController {
  signup = async (req: Request, res: Response): Promise<Response> => {
    let { name, nim, email, password } = req.body
    const hashedPassword: string = await Authentication.passwordHash(password)
    const user = new User({ name, nim, email, password: hashedPassword })

    let token = Authentication.generateToken(user.email, user.password)
    await user.save()
    return res.send({
      success: true,
      messages: 'OK',
      data: { token },
    })
  }

  signin = async (req: Request, res: Response): Promise<Response> => {
    let { email } = req.body
    const services: UserService = new UserService(req)
    const user = new User(await services.getUserByEmail(email))
    let token = Authentication.generateToken(user.email, user.password) //generate jwt-token

    return res.send({
      success: true,
      messages: 'OK',
      data: { token },
    })
  }

  ukmSignin = async (req: Request, res: Response): Promise<Response> => {
    let { username } = req.body
    const services: CommunityService = new CommunityService(req)
    const community = new Community(await services.findByUsername(username))
    let token = Authentication.generateToken(community.username, community.password) //generate jwt-token

    return res.send({
      success: true,
      messages: 'OK',
      data: { token },
    })
  }
}

export default new authController()
