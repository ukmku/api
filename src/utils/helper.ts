import mongoose from 'mongoose'
import _ from 'lodash'
import Personality from '../models/Personality'

class Helper {
  public static isValid = (id: String | any): boolean => {
    var isValid = mongoose.Types.ObjectId.isValid(id)
    return isValid
  }

  public static compressedArray = (originalArr: Array<String>): any => {
    let compressed: any[] = []
    let sortedArr = originalArr.sort()
    let copyArr: any[] = sortedArr.slice(0)

    for (let i = 0; i < sortedArr.length; i++) {
      let myCount: number = 0
      // loop over every element in the copy and see if it's the same
      for (let j = 0; j < copyArr.length; j++) {
        if (sortedArr[i] === copyArr[j]) {
          // increase amount of times duplicate is found
          myCount++
          // sets item to undefined
          delete copyArr[j]
        }
      }

      if (myCount > 0) {
        let result: any = new Object()
        result.count = myCount
        result.value = sortedArr[i]
        compressed.push(result)
      }
    }

    return compressed
  }

  public static compareValues(key: any, order = 'asc') {
    return function innerSort(a: any, b: any) {
      if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
        // property doesn't exist on either object
        return 0
      }

      const varA = typeof a[key] === 'string' ? a[key].toUpperCase() : a[key]
      const varB = typeof b[key] === 'string' ? b[key].toUpperCase() : b[key]

      let comparison = 0
      if (varA > varB) {
        comparison = 1
      } else if (varA < varB) {
        comparison = -1
      }
      return order === 'desc' ? comparison * -1 : comparison
    }
  }

  public static getRowPersonality = (value: string): Array<number> | any => {
    let data: number[][] = [
      [1, 1, 3, 5, 1, 5, 3, 5],
      [3, 5, 1, 3, 1, 5, 1, 1],
      [1, 1, 1, 5, 1, 3, 1, 5],
      [1, 5, 3, 3, 1, 5, 1, 1],
      [1, 5, 1, 1, 1, 1, 5, 1],
      [1, 5, 1, 1, 5, 3, 3, 1],
      [1, 1, 5, 3, 5, 1, 3, 3],
      [3, 5, 1, 3, 5, 1, 1, 1],
      [5, 1, 3, 1, 1, 5, 1, 5],
      [5, 1, 3, 3, 1, 5, 1, 5],
      [5, 1, 1, 1, 1, 3, 1, 3],
      [3, 1, 1, 1, 3, 5, 3, 3],
      [1, 3, 1, 3, 3, 1, 5, 1],
      [1, 1, 5, 5, 1, 1, 5, 1],
      [1, 1, 3, 3, 1, 1, 3, 3],
      [1, 3, 1, 3, 3, 1, 3, 1],
    ]

    let dataRow: number[]

    if (value === 'INTJ') {
      dataRow = data[0]
    } else if (value === 'INTP') {
      dataRow = data[1]
    } else if (value === 'ENTJ') {
      dataRow = data[2]
    } else if (value === 'ENTP') {
      dataRow = data[3]
    } else if (value === 'ISFJ') {
      dataRow = data[4]
    } else if (value === 'ISFP') {
      dataRow = data[5]
    } else if (value === 'ESFJ') {
      dataRow = data[6]
    } else if (value === 'ESFP') {
      dataRow = data[7]
    } else if (value === 'ISTJ') {
      dataRow = data[8]
    } else if (value === 'ISTP') {
      dataRow = data[9]
    } else if (value === 'ESTJ') {
      dataRow = data[10]
    } else if (value === 'ESTP') {
      dataRow = data[11]
    } else if (value === 'INFJ') {
      dataRow = data[12]
    } else if (value === 'INFP') {
      dataRow = data[13]
    } else if (value === 'ENFJ') {
      dataRow = data[14]
    } else if (value === 'ENFP') {
      dataRow = data[15]
    } else {
      return 'Personality is wrong'
    }

    return dataRow
  }

  public static getRowInterest = (value: string): Array<number> | any => {
    let data: number[][] = [
      [5, 3, 1, 1, 5, 1, 1, 1],
      [1, 1, 3, 5, 1, 5, 1, 5],
      [1, 5, 1, 5, 3, 1, 3, 1],
      [3, 3, 5, 3, 3, 1, 5, 3],
      [5, 1, 5, 1, 1, 1, 3, 1],
      [3, 5, 1, 3, 5, 3, 3, 5],
    ]

    let dataRow: number[]

    if (value === 'Realistis') {
      dataRow = data[0]
    } else if (value === 'Investigatif') {
      dataRow = data[1]
    } else if (value === 'Artistik') {
      dataRow = data[2]
    } else if (value === 'Sosial') {
      dataRow = data[3]
    } else if (value === 'Konvensional') {
      dataRow = data[4]
    } else if (value === 'Enterprising') {
      dataRow = data[5]
    } else {
      return 'Interest is wrong'
    }
    return dataRow
  }

  public static getRowDepartment = (value: string): Array<number> | any => {
    let data: number[][] = [
      [3, 3, 5, 3, 5, 5, 5, 3],
      [3, 3, 5, 3, 5, 5, 5, 3],
      [3, 3, 5, 3, 5, 5, 5, 3],
      [3, 5, 5, 5, 5, 3, 5, 3],
      [3, 5, 5, 5, 5, 3, 5, 3],
      [3, 5, 5, 5, 5, 3, 5, 3],
      [3, 3, 5, 5, 3, 3, 5, 5],
      [3, 3, 5, 3, 3, 3, 5, 5],
      [3, 3, 5, 3, 3, 3, 5, 5],
    ]

    let dataRow: number[]
    if (value === 'S1 Sistem Informasi') {
      dataRow = data[0]
    } else if (value === 'S1 Teknik Komputer') {
      dataRow = data[1]
    } else if (value === 'D3 Sistem Informasi') {
      dataRow = data[2]
    } else if (value === 'D4 Produksi Film dan Televisi') {
      dataRow = data[3]
    } else if (value === 'S1 Desain Komunikasi Visual') {
      dataRow = data[4]
    } else if (value === 'S1 Desain Produk') {
      dataRow = data[5]
    } else if (value === 'D3 Administrasi Perkantoran') {
      dataRow = data[6]
    } else if (value === 'S1 Akuntansi') {
      dataRow = data[7]
    } else if (value === 'S1 Manajemen') {
      dataRow = data[8]
    } else {
      return 'Department is wrong'
    }

    return dataRow
  }

  public static getMinMax = (
    value: {
      name: string
      personality: number
      interestOne: number
      interestTwo: number
      interestThree: number
      department: number
    }[]
  ): any => {
    const maxPersonality = _.maxBy(value, 'personality')
    const maxInterestOne = _.maxBy(value, 'interestOne')
    const maxInterestTwo = _.maxBy(value, 'interestTwo')
    const maxInterestThree = _.maxBy(value, 'interestThree')
    const maxDepartment = _.maxBy(value, 'department')

    return {
      personality: maxPersonality?.personality,
      interestOne: maxInterestOne?.interestOne,
      interestTwo: maxInterestTwo?.interestTwo,
      interestThree: maxInterestThree?.interestThree,
      department: maxDepartment?.department,
    }
  }

  public static normalization = (matrix: any, maxmin: any): any => {
    matrix.personality = matrix.personality / maxmin.personality
    matrix.interestOne = matrix.interestOne / maxmin.interestOne
    matrix.interestTwo = matrix.interestTwo / maxmin.interestTwo
    matrix.interestThree = matrix.interestThree / maxmin.interestThree
    matrix.department = matrix.department / maxmin.department

    return matrix
  }

  public static rangking(score: any) {
    const criteria = {
      personality: 0.45,
      interestOne: 0.15,
      interestTwo: 0.15,
      interestThree: 0.15,
      department: 0.1,
    }
    const total = (score.personality * criteria.personality) + (score.interestOne * criteria.interestOne) + (score.interestTwo * criteria.interestTwo) + (score.interestThree * criteria.interestThree) + (score.department * criteria.department);
    const result = {
        name: score.name,
        total: total
    }
    return result;
}
}

export default Helper
