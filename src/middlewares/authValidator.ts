import { NextFunction, Request, Response } from 'express'
import User, { IUser } from '../models/User'
import { check, validationResult } from 'express-validator'

import Authentication from '../utils/authentication'
import Community from '../models/Community'

export const validateSignup = [
  check('name').isString(),
  check('nim')
    .isString()
    .isLength({ min: 11, max: 11 })
    .matches(/(^\d{11})+$/)
    .withMessage(
      'Nim invalid, must be only containing number, anda 11 characters'
    ),
    // .custom(async (value: Object) => {
    //   const user = await User.findOne({ nim: value })
    //   if (user) {
    //     return Promise.reject('Nim already in use')
    //   }
    // }),
  check('email')
    .isEmail()
    .custom(async (value: Object) => {
      const user = await User.findOne({ email: value })
      if (user) {
        return Promise.reject('Email already in use')
      }
    }),
  check('password')
    .isString()
    .isLength({ min: 8 })
    .matches(/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[^\W\d\s:])([^\s]){8,16}$/)
    .withMessage(
      'Password invalid, must be containing lowercase, uppercase, number, and min:8 characters'
    )
    .custom(async (value: Object, { req: Request }) => {
      if (value !== Request.body.passwordConfirmation) {
        throw new Error('Password confirmation is incorrect')
      }
    }),
  (req: Request, res: Response, next: NextFunction) => {
    const err = validationResult(req)

    if (!err.isEmpty()) {
      return res.status(422).send({ errors: err.array() })
    }

    return next()
  },
]

export const validateSignin = [
  check('email')
    .isEmail()
    .custom(async (value: Object) => {
      const user = await User.findOne({ email: value })
      if (!user) {
        return Promise.reject('Credentials not Match')
      }
    }),
  check('password')
    .isLength({ min: 8 })
    .custom(async (value: string, { req: Request }) => {
      const user = new User(await User.findOne({ email: Request.body.email }))
      const compare = await Authentication.passwordCompare(value, user.password)
      if (!compare) {
        throw new Error('Credentials not Match')
      }
    }),

  (req: Request, res: Response, next: NextFunction) => {
    const err = validationResult(req)

    if (!err.isEmpty()) {
      return res.status(422).send({ errors: err.array() })
    }

    return next()
  },
]

export const communitySignin = [
  check('username')
    .isString()
    .custom(async (value: Object) => {
      const community = await Community.findOne({ username: value })
      if (!community) {
        return Promise.reject('Credentials not Match')
      }
    }),
  check('password')
    .isLength({ min: 8 })
    .custom(async (value: string, { req: Request }) => {
      const community = new User(
        await Community.findOne({ username: Request.body.username })
      )
      const compare = await Authentication.passwordCompare(
        value,
        community.password
      )
      if (!compare) {
        throw new Error('Credentials not Match')
      }
    }),

  (req: Request, res: Response, next: NextFunction) => {
    const err = validationResult(req)

    if (!err.isEmpty()) {
      return res.status(422).send({ errors: err.array() })
    }

    return next()
  },
]

export default { validateSignup, validateSignin }
