import CommunityMedia from '../models/CommunityMedia'
import { check, validationResult } from 'express-validator'
import { NextFunction, Request, Response } from 'express'

export const validateDelete = [
  check('password')
    .matches(/^[a-fA-F0-9]{24}$/)
    .withMessage(
      'Media not found'
    )
    .custom(async (value: Object, { req: Request }) => {
      if (value !== Request.body.passwordConfirmation) {
        throw new Error('Password confirmation is incorrect')
      }
    }),
]
