import CommunityCategory from '../models/CommunityCategory'
import { check, validationResult } from 'express-validator'
import { NextFunction, Request, Response } from 'express'

export const validateStore = [
  check('name')
    .isString()
    .custom(async (value: Object) => {
      const data = await CommunityCategory.findOne({ name: value })
      if (data) {
        return Promise.reject('Category name already in use')
      }
    }),
  (req: Request, res: Response, next: NextFunction) => {
    const err = validationResult(req)

    if (!err.isEmpty()) {
      return res.status(422).send({ errors: err.array() })
    }

    return next()
  },
]
