import Community from '../models/Community'
import { check, validationResult } from 'express-validator'
import { NextFunction, Request, Response } from 'express'

export const validateStore = [
  check('username')
    .isString()
    .isLength({ min: 5 })
    .withMessage(
      'Username invalid, min:5 characters'
    )
    .custom(async (value: Object) => {
      const data = await Community.findOne({ username: value })
      if (data) {
        return Promise.reject('username already in use')
      }
    }),
  check('password')
    .isString()
    .isLength({ min: 8 })
    .matches(/^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[^\W\d\s:])([^\s]){8,16}$/)
    .withMessage(
      'Password invalid, must be containing lowercase, uppercase, number, and min:8 characters'
    )
    .custom(async (value: Object, { req: Request }) => {
      if (value !== Request.body.passwordConfirmation) {
        throw new Error('Password confirmation is incorrect')
      }
    }),
  check('name')
    .isString()
    .custom(async (value: Object) => {
      const data = await Community.findOne({ name: value })
      if (data) {
        return Promise.reject('Community name already in use')
      }
    }),
  check('category').isString(),
  (req: Request, res: Response, next: NextFunction) => {
    const err = validationResult(req)

    if (!err.isEmpty()) {
      return res.status(422).send({ errors: err.array() })
    }

    return next()
  },
]
