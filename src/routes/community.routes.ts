import communityController from '../controllers/community.controller'
import { validateStore } from '../middlewares/communityValidator'
import { auth, communityAuth } from '../middlewares/authMiddleware'
import baseRoutes from './utils/baseRoutes'
import communityMediaController from '../controllers/communityMedia.controller'

class communityCategoryRoutes extends baseRoutes {
  public routes(): void {
    this.router.get('/', auth, communityController.index)
    this.router.post('/', auth, validateStore, communityController.store)
    this.router.get('/:id', auth, communityController.show)
    this.router.delete('/:id', auth, communityController.destroy)

    this.router.get(
      '/category/:category',
      auth,
      communityController.getByCategory
    )

    // community media
    this.router.get('/media', communityAuth, communityMediaController.index)
    this.router.post('/media', communityAuth, communityMediaController.store)
    this.router.get('/media/:id', communityAuth, communityMediaController.show)
    this.router.put(
      '/media/:id',
      communityAuth,
      communityMediaController.update
    )
    this.router.delete(
      '/media/:id',
      communityAuth,
      communityMediaController.destroy
    )
    this.router.get(
      '/media/me',
      communityAuth,
      communityMediaController.getByAuth
    )
    this.router.get(
      '/media/me/video',
      communityAuth,
      communityMediaController.showVideo
    )
    this.router.get(
      '/media/me/image',
      communityAuth,
      communityMediaController.showImage
    )

    // community management
    this.router.get('/profile/me', communityAuth, communityController.profile)
    this.router.put('/', communityAuth, communityController.update)
  }
}

export default new communityCategoryRoutes().router
