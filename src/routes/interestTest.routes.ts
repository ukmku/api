import { auth } from '../middlewares/authMiddleware'
import baseRoutes from './utils/baseRoutes'
import interestTestController from '../controllers/interestTest.controllers'

class interestTestRoutes extends baseRoutes {
  public routes(): void {
    this.router.get('/', interestTestController.indexRandom)
    this.router.get('/:id', interestTestController.showByModul)
  }
}

export default new interestTestRoutes().router
