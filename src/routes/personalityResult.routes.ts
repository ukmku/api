import { auth } from "../middlewares/authMiddleware";
import baseRoutes from "./utils/baseRoutes";
import personanalityResultController from "../controllers/personalityResult.controllers";
import personalityResultControllers from "../controllers/personalityResult.controllers";

class personalityRoutes extends baseRoutes {
    public routes(): void {
        this.router.get("/", auth, personanalityResultController.index)
        this.router.post("/", auth, personanalityResultController.store);
        this.router.get("/:id" , auth, personanalityResultController.show);
        this.router.get("/personality/me", auth, personalityResultControllers.myPersonality)
    }
}

export default new personalityRoutes().router;
