import { auth } from '../middlewares/authMiddleware'
import baseRoutes from './utils/baseRoutes'
import interestResultController from '../controllers/interestResult.controller'

class personalityRoutes extends baseRoutes {
  public routes(): void {
    // this.router.get('/', auth, personanalityResultController.index)
    this.router.post('/', auth, interestResultController.store)
    // this.router.get('/:id', auth, personanalityResultController.show)
    // this.router.get(
    //   '/myPersonality',
    //   auth,
    //   personanalityResultController.myPersonality
    // )
  }
}

export default new personalityRoutes().router
