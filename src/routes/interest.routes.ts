// Dependency

import { auth } from '../middlewares/authMiddleware'
import baseRoutes from './utils/baseRoutes'
import interestController from '../controllers/interest.controller'

class personalityRoutes extends baseRoutes {
  public routes(): void {
    this.router.get('/', auth, interestController.index)
    this.router.get('/:slug', auth, interestController.show)
    this.router.get('/three/me', interestController.getThree)

  }
}

export default new personalityRoutes().router
