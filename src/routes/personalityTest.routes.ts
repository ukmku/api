import { auth } from "../middlewares/authMiddleware";
import baseRoutes from "./utils/baseRoutes";
import personalityTestController from "../controllers/personalityTest.controllers";

class personalityRoutes extends baseRoutes {
    public routes(): void {
        this.router.get("/", personalityTestController.indexRandom);
        this.router.get("/:id", personalityTestController.showByModul); 
    }
}

export default new personalityRoutes().router;
