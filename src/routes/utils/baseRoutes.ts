import IRouter from "./interfaceRoutes";
import { Router } from "express";

abstract class baseRoutes implements IRouter {
    public router: Router;

    constructor() {
        this.router = Router();
        this.routes();
    }

    abstract routes(): void;

}

export default baseRoutes;
