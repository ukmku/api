import communityCategoryController from '../controllers/communityCategory.controller'
import { validateStore } from '../middlewares/communityCategoryValidator'
import { auth } from '../middlewares/authMiddleware'
import baseRoutes from './utils/baseRoutes'

class communityCategoryRoutes extends baseRoutes {
  public routes(): void {
    this.router.get('/', auth, communityCategoryController.index)
    this.router.post('/', auth, validateStore, communityCategoryController.store)
    this.router.get('/:id', auth, communityCategoryController.show)
    this.router.put('/:id', auth, validateStore, communityCategoryController.update)
    this.router.delete('/:id', auth, communityCategoryController.delete)
  }
}

export default new communityCategoryRoutes().router
