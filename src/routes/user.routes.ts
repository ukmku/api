import { auth } from '../middlewares/authMiddleware'
import baseRoutes from './utils/baseRoutes'
import userController from '../controllers/user.controllers'

// Controllers

class userRoutes extends baseRoutes {
  public routes(): void {
    this.router.get('/profile', auth, userController.profile)
    this.router.put('/profile', auth, userController.update)
    this.router.put('/profile/avatar', auth, userController.avatar)
  }
}

export default new userRoutes().router
