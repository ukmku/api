// Dependency

import { auth } from "../middlewares/authMiddleware";
import baseRoutes from "./utils/baseRoutes";
import personalityController from "../controllers/personality.controllers";

class personalityRoutes extends baseRoutes {
    public routes(): void {
        this.router.get("/", auth, personalityController.index);
        this.router.get("/:acronym", auth, personalityController.show);
    }
}

export default new personalityRoutes().router;
