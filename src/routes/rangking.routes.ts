import { auth } from '../middlewares/authMiddleware'
import baseRoutes from './utils/baseRoutes'
import rangkingController from '../controllers/rangking.controller'

class personalityRoutes extends baseRoutes {
  public routes(): void {
    this.router.post('/', auth, rangkingController.store)
  }
}

export default new personalityRoutes().router
