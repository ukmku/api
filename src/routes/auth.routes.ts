import { validateSignin, validateSignup, communitySignin } from "../middlewares/authValidator";

import { auth } from "../middlewares/authMiddleware";
import authController from "../controllers/auth.controllers";
import baseRoutes from "./utils/baseRoutes";

// Controllers


class authRoutes extends baseRoutes {
    public routes(): void {
        this.router.post("/signup", validateSignup, authController.signup);
        this.router.post("/signin", validateSignin, authController.signin);
        this.router.post("/community/signin", communitySignin, authController.ukmSignin);
    }
}

export default new authRoutes().router;
