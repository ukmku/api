import PersonalityTest, {
  IPersonalityTest,
} from '../models/PersonalityTest'

import { Request } from 'express'

class PersonalityTestService {
  auth: { id: string }
  body: Request['body']
  params: Request['params']

  constructor(req: Request) {
    this.auth = req.app.locals.user
    this.body = req.body
    this.params = req.params
  }

  getAll = async () => {
    const personalityTest = await PersonalityTest.find({})

    return personalityTest
  }

  showByModul = async () => {
    const { id } = this.params
    const personalityTest = await PersonalityTest.find({ 'modul.id': id })

    return personalityTest
  }
}

export default PersonalityTestService
