import User, { IUser } from '../models/User'

import { Request } from 'express'

class UserService {
  auth: { id: string }
  body: Request['body']
  params: Request['params']

  constructor(req: Request) {
    this.auth = req.app.locals.user
    this.body = req.body
    this.params = req.params
  }

  Auth = async () => {
    const user = await User.findOne(this.auth)
    return user
  }

  getUserByName = async (name: string) => {
    const user = await User.findOne({ name: name })
    return user
  }

  getUserByEmail = async (email: string) => {
    const user = await User.findOne({ email: email })
    return user
  }

  updateAvatar = async (_id: string, avatar: string) => {
    const user = await User.findOneAndUpdate(
      { _id },
      {
        $set: {
          avatar,
        },
      }
    )

    return user
  }

  update = async (
    _id: string,
    nim: string,
    name: string,
    department: string,
    phone: string
  ) => {
    const user = await User.findOneAndUpdate(
      { _id },
      {
        $set: {
          nim,
          name,
          department,
          phone,
        },
      }
    )

    return user
  }

  // store = async () => {
  //     const personality = new Personality(this.body);
  //     await personality.save();

  //     return personality;
  // }

  // show = async () => {
  //     const { acronym } = this.params;
  //     const personality = await Personality.findOne({ "acronym": acronym });

  //     return personality;
  // }

  // update = async () => {
  //     const { id } = this.params;
  //     const personality = await Personality.findOneAndUpdate({ _id: id }, { $set: this.body });

  //     return personality;
  // }

  // destroy = async () => {
  //     const { id } = this.params;
  //     const personality = await Personality.findOneAndDelete({ _id: id });

  //     return personality;
  // }
}

export default UserService
