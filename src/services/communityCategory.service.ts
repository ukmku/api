import CommunityCategory from '../models/CommunityCategory'
import { Request } from 'express'

class CommunityCategoryService {
  auth: { id: string }
  body: Request['body']
  params: Request['params']

  constructor(req: Request) {
    this.auth = req.app.locals.user
    this.body = req.body
    this.params = req.params
  }

  index = async () => {
    const communityCategory = await CommunityCategory.find({})
    return communityCategory
  }

  store = async (name: string) => {
    const communityCategory = new CommunityCategory({ name })
    await communityCategory.save()
    return communityCategory
  }

  show = async () => {
    const { id } = this.params
    const communityCategory = await CommunityCategory.findById({ _id: id })
    return communityCategory
  }

  update = async () => {
    const { id } = this.params
    const { name } = this.body
    const communityCategory = await CommunityCategory.update(
      { _id: id },
      { name }
    )
    return communityCategory
  }

  destroy = async () => {
    const { id } = this.params
    const communityCategory = await CommunityCategory.deleteOne({ _id: id })
    return communityCategory
  }
}

export default CommunityCategoryService
