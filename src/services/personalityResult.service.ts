import PersonalityResult from '../models/PersonalityResult'
import { Request } from 'express'

class PersonalityResultService {
  auth: { id: string }
  body: Request['body']
  params: Request['params']

  constructor(req: Request) {
    this.auth = req.app.locals.user
    this.body = req.body
    this.params = req.params
  }

  getAll = async () => {
    const personalityResult = await PersonalityResult.find({})
    return personalityResult
  }

  showById = async (id: string) => {
    const personalityResult = await PersonalityResult.findOne({ _id: id })
    return personalityResult
  }

  myPersonality = async () => {
    const { id } = this.auth
    const myPersonality = await PersonalityResult.findOne({ createdBy: id })

    return myPersonality
  }

  store = async (
    createdBy: string,
    personality: string,
    personalityReport: [{ count: number; value: string }]
  ) => {
    const personalityResult = new PersonalityResult({
      createdBy,
      personality,
      personalityReport,
    })
    await personalityResult.save()

    return personalityResult
  }

  update = async (
    createdBy: string,
    personality: string,
    personalityReport: [{ count: number; value: string }]
  ) => {
    const { id } = this.auth
    const personalityResult = PersonalityResult.findOneAndUpdate(
      { createdBy: id },
      { createdBy, personality, personalityReport }
    )
    return personalityResult
  }
}

export default PersonalityResultService
