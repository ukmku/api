import Authentication from '../utils/authentication'
import Community, { ICommunity } from '../models/Community'
import { Request } from 'express'
import CommunityMedia from '../models/CommunityMedia'

class CommunityService {
  auth: { id: string }
  body: Request['body']
  params: Request['params']
  protocol: Request['protocol']
  host: any

  constructor(req: Request) {
    this.auth = req.app.locals.community
    this.body = req.body
    this.params = req.params
    this.protocol = req.protocol
    this.host = req.get('host')
  }

  Auth = async () => {
    const user = await Community.findOne({ _id: this.auth })
    return user
  }

  index = async () => {
    const community = await Community.find({})
    return community
  }

  findByCategory = async (category: string) => {
    const community = await Community.find({ category })
    return community
  }

  findByUsername = async (username: string) => {
    const community = await Community.findOne({ username })
    return community
  }

  store = async () => {
    let { username, password, name, category } = this.body
    const hashedPassword: string = await Authentication.passwordHash(password)
    const community = new Community({
      username,
      name,
      category,
      password: hashedPassword,
    })
    await community.save()
    return community
  }

  update = async (
    logo: string,
    description: string,
    schedule: string,
    activities: string,
    achievements: string,
    contactPerson: string,
    email: string,
    instagram: string,
    twitter: string,
    youtube: string,
    facebook: string,
    website: string
  ) => {
    const community = await Community.update(
      { _id: this.auth },
      {
        logo,
        description,
        schedule,
        activities,
        achievements,
        contactPerson,
        email,
        instagram,
        twitter,
        youtube,
        facebook,
        website,
      }
    )
    return community
  }

  show = async () => {
    const { id } = this.params
    const community = new Community(await Community.findById({ _id: id }))

    const communityObj = (community: ICommunity) => {
      if (community.logo === null) {
        community.logo = 'null'
      } else {
        community.logo = `${this.protocol}://${this.host}${community.logo}`
      }
      return community
    }

    const video = await CommunityMedia.find({ createdBy: id })
      .where('video')
      .ne(null)

    const image = await CommunityMedia.find({ createdBy: id })
      .where('image')
      .ne(null)

    const mediaMapped = image.map((data) => {
      if (typeof data.image === 'undefined') {
        data.image = ''
      } else {
        data.image = `${this.protocol}://${this.host}${data.image}`
      }
      return data
    })

    const a = { A: 'S' }
    let ukm = Object.assign(
      { community: communityObj(community) },
      { video: video },
      { image: mediaMapped }
    )
    return ukm
  }

  destroy = async () => {
    const { id } = this.params
    const community = await Community.deleteOne({ _id: id })
    return community
  }
}

export default CommunityService
