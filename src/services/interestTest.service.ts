import InterestTest from '../models/InterestTest'

import { Request } from 'express'

class PersonalityService {
  auth: { id: string }
  body: Request['body']
  params: Request['params']

  constructor(req: Request) {
    this.auth = req.app.locals.user
    this.body = req.body
    this.params = req.params
  }

  getAll = async () => {
    const interest = await InterestTest.find()
    return interest
  }

  showByModul = async () => {
    const { id } = this.params
    const interestTest = await InterestTest.find({ modul_id: id })

    return interestTest
  }

  countDocument = async (modul: string) => {
    const result = await InterestTest.countDocuments({ modul })
    return result
  }
}

export default PersonalityService
