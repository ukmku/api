import Interest, { IInterest } from '../models/Interest'

import { Request } from 'express'

class PersonalityService {
  auth: { id: string }
  body: Request['body']
  params: Request['params']

  constructor(req: Request) {
    this.auth = req.app.locals.user
    this.body = req.body
    this.params = req.params
  }

  getAll = async () => {
    const interest = await Interest.find()
    return interest
  }

  getThree = async (arr: Array<string>) => {
    const [one, two, three] = arr
    const interest = await Interest.find({
      $or: [{ slug: one }, { slug: two }, { slug: three }],
    })
    return interest
  }

  show = async () => {
    const { slug } = this.params
    const interest = await Interest.findOne({ slug })

    return interest
  }
}

export default PersonalityService
