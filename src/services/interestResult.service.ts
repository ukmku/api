import InterestResult, { IInterestResult } from '../models/InterestResults'

import { Request } from 'express'
import interestTestRoutes from '../routes/interestTest.routes'

class InterestResultService {
  auth: { id: string }
  body: Request['body']
  params: Request['params']

  constructor(req: Request) {
    this.auth = req.app.locals.user
    this.body = req.body
    this.params = req.params
  }

  getAll = async () => {
    const result = await InterestResult.find({})
    return result
  }

  showById = async (id: string) => {
    const result = await InterestResult.findOne({ _id: id })
    return result
  }

  myInterest = async () => {
    const { id } = this.auth
    const myInterest = await InterestResult.findOne({ createdBy: id })

    return myInterest
  }
}

export default InterestResultService
