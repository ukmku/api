import Personality, { IPersonality } from '../models/Personality'

import { Request } from 'express'

class PersonalityService {
  auth: { id: string }
  body: Request['body']
  params: Request['params']

  constructor(req: Request) {
    this.auth = req.app.locals.user
    this.body = req.body
    this.params = req.params
  }

  getAll = async () => {
    const personality = await Personality.find()
    return personality
  }

  show = async () => {
    const { acronym } = this.params
    const personality = await Personality.findOne({ acronym })

    return personality
  }
}

export default PersonalityService
