import mongoose, { Document, Schema } from 'mongoose'
import User, { IUser } from './User'

export interface ICommunityMedia extends Document {
  createdBy: IUser['_id']
  image: string
  video: string
  title: string
  description: string
}

const communityMedia = new Schema(
  {
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    image: {
      type: String,
      trim: true,
      default: null,
    },
    video: {
      type: String,
      trim: true,
      default: null,
    },
    title: {
      type: String,
      trim: true,
      default: null,
    },
    description: {
      type: String,
      trim: true,
      default: null,
    },
  },
  {
    timestamps: true,
  }
)

export default mongoose.model<ICommunityMedia>(
  'Community_media',
  communityMedia
)
