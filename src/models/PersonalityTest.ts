import mongoose, { Document, Schema } from 'mongoose'

import mongoose_random from 'mongoose-simple-random'

// Interface
export interface IPersonalityTest extends Document {
  modul: {
    id: string
    namae: string
  }
  statement: {
    first: {
      personality: string
      desc: string
    }
    second: {
      personality: string
      desc: string
    }
  }
}

const personalityTestSchema = new Schema({
  modul: {
    id: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      required: true,
      trim: true,
    },
  },
  statement: {
    first: {
      personality: {
        type: String,
        required: true,
        trim: true,
      },
      desc: {
        type: String,
        required: true,
        trim: true,
      },
    },
    second: {
      personality: {
        type: String,
        required: true,
        trim: true,
      },
      desc: {
        type: String,
        required: true,
        trim: true,
      },
    },
  },
})

personalityTestSchema.plugin(mongoose_random)

export default mongoose.model<IPersonalityTest>(
  'Personality_test',
  personalityTestSchema
)
