import User, { IUser } from './User'
import Interest, { IInterest } from './Interest'
import mongoose, { Document, Schema } from 'mongoose'

export interface IInterestResult extends Document {
  createdBy: IUser['_id']
  interest: Array<string>
  description: IInterest['_id']
  interestReport: [
    {
      count: number
      value: string
    }
  ]
}

const interestResultSchema = new Schema(
  {
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    interest: [],
    description: [],
    interestReport: [],
  },
  {
    timestamps: true,
  }
)

export default mongoose.model<IInterestResult>(
  'Interest_result',
  interestResultSchema
)
