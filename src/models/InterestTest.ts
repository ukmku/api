import mongoose, { Document, Schema } from 'mongoose'
import mongoose_random from 'mongoose-simple-random'

// Interface
export interface IInterestTest extends Document {
  image: string
  name: string
  description: string
  characteristic: Array<string>
}

const interestTestSchema = new Schema({
  statement: {
    type: String,
    required: true,
    trim: true,
  },
  modul_id: {
    type: String,
    required: true,
    trim: true,
  },
  modul: {
    type: String,
    required: true,
    trim: true,
  },
})

interestTestSchema.plugin(mongoose_random)
export default mongoose.model<IInterestTest>('Interest_test', interestTestSchema)
