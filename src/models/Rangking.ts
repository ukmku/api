import User, { IUser } from './User'
import mongoose, { Document, Schema } from 'mongoose'

export interface IRangking extends Document {
  createdBy: IUser['_id']
  rangking: [{ name: string; value: string }]
}

const rangkingSchema = new Schema(
  {
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    ranking: [],
  },
  {
    timestamps: true,
  }
)

export default mongoose.model<IRangking>('Rangking', rangkingSchema)
