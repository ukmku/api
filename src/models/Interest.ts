import mongoose, { Document, Schema } from 'mongoose'

// Interface
export interface IInterest extends Document {
  image: string
  name: string
  description: string
  characteristic: Array<string>
}

const interestSchema = new Schema({
  image: {
    type: String,
    required: true,
    trim: true,
  },
  name: {
    type: String,
    required: true,
    trim: true,
  },
  description: {
    type: String,
    required: true,
    trim: true,
  },
  characteristic: [],
})

export default mongoose.model<IInterest>('Interest', interestSchema)
