import mongoose, { Document, Schema } from 'mongoose'

export interface ICommunity extends Document {
  logo: string
  name: string
  username: string
  category: string
  description: string
  schedule: string
  activities: string
  achievements: string
  contactPerson: string
  instagram: string
  twitter: string
  youtube: string
  facebook: string
  website: string
  email:  string
  password: string
  video: any
}

const communitySchema = new Schema(
  {
    logo: {
      type: String,
      trim: true,
      default: null,
    },
    username: {
      type: String,
      required: true,
      trim: true,
      lowercase: true
    },
    name: {
      type: String,
      required: true,
      trim: true,
    },
    category: {
      type: String,
      required: true,
      trim: true,
    },
    description: {
      type: String,
      trim: true,
    },
    schedule: {
      type: String,
      trim: true,
    },
    activities: {
      type: String,
      trim: true,
    },
    achievements: {
      type: String,
      trim: true,
    },
    contactPerson: {
      type: String,
      trim: true,
    },
    instagram: {
      type: String,
      trim: true,
    },
    twitter: {
      type: String,
      trim: true,
    },
    youtube: {
      type: String,
      trim: true,
    },
    facebook: {
      type: String,
      trim: true,
    },
    website: {
      type: String,
      trim: true,
    },
    email: {
      type: String,
      trim: true,
    },
    password: {
      type: String,
      required: true,
      trim: true,
    },
  },
  {
    timestamps: true,
  }
)

export default mongoose.model<ICommunity>('Community', communitySchema)
