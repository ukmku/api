import User, { IUser } from './User'
import mongoose, { Document, Schema } from 'mongoose'

export interface IPersonalityResult extends Document {
  createdBy: IUser['_id']
  personality: string
  personalityReport: [
    {
      count: number
      value: string
    }
  ]
}

const personalityResultSchema = new Schema(
  {
    createdBy: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    personality: {
      type: String,
      trim: true,
    },
    personalityReport: [],
  },
  {
    timestamps: true,
  }
)

export default mongoose.model<IPersonalityResult>(
  'Personality_result',
  personalityResultSchema
)
