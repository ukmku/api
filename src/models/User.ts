import mongoose, { Document, Schema } from 'mongoose'

export interface IUser extends Document {
  avatar: string
  email: string
  password: string
  nim: string
  name: string
  department: string
  phone: string
}

const userSchema = new Schema(
  {
    avatar: {
      type: String,
      trim: true,
      default: null,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      lowercase: true,
      trim: true,
    },
    password: {
      type: String,
      required: true,
      trim: true,
    },
    nim: {
      type: String,
      trim: true,
    },
    name: {
      type: String,
      required: true,
      lowercase: true,
      trim: true,
    },
    department: {
      type: String,
      trim: true,
    },
    phone: {
      type: String,
      trim: true,
    },
  },
  {
    timestamps: true,
  }
)

userSchema.methods.toJSON = function () {
  let obj = this.toObject()
  delete obj.password
  delete obj.createdAt
  delete obj.updatedAt
  delete obj.__v
  return obj
}

export default mongoose.model<IUser>('User', userSchema)
