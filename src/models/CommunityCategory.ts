import mongoose, { Document, Schema } from 'mongoose'

export interface ICommunityCategory extends Document {
  name: string
}

const communityCategory = new Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
  },
  {
    timestamps: true,
  }
)

export default mongoose.model<ICommunityCategory>(
  'Community_category',
  communityCategory
)
