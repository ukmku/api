import mongoose, { Document, Schema } from 'mongoose'

// Interface
export interface IPersonality extends Document {
  image: string
  fullname: string
  acronym: string
  nickname: string
  narrative: string
  characteristic: Array<string>
  developmentAdvice: Array<string>
  profession: string
  partner: Array<string>
  publicFigure: Array<string>
}

const personalitySchema = new Schema({
  image: {
    type: String,
    required: true,
    trim: true,
  },
  fullname: {
    type: String,
    required: true,
    trim: true,
  },
  acronym: {
    type: String,
    required: true,
    trim: true,
  },
  nickname: {
    type: String,
    required: true,
    trim: true,
  },
  narrative: {
    type: String,
    required: true,
    trim: true,
  },
  characteristic: [],
  developmentAdvice: [],
  profession: {
    type: String,
    required: true,
    trim: true,
  },
  partner: [],
  publicFigure: [],
})

export default mongoose.model<IPersonality>('Personality', personalitySchema)
